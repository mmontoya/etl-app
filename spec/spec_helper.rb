ENV['RAILS_ENV'] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rails/all'
require 'rspec/rails'
require 'factory_bot_rails'


RSpec.configure do |config|
  config.mock_with :rspec
  config.color = true

end

