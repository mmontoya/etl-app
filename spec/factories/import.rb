FactoryBot.define do
  factory :import do
    notes    { 'note' }
    sumcheck { 'sumcheck' }
    file     { 'employees.csv' }
    done     { false }

    after(:build) do |import|
      import.files.attach(io: File.open(Rails.root.join(
                                          'spec', 'support',
                                          'files','employees.csv')),
                          filename: 'employees.csv',
                          content_type: 'text/csv')
    end
  end
end
