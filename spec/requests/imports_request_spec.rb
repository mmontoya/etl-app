require 'spec_helper'

RSpec.describe ImportsController, type: :request do

  let!(:import)  { FactoryBot.create :import }

  describe "GET#etl_process" do
    it "returns a successful 200 response for process action" do

      get etl_process_import_path import

      expect(response).to redirect_to(imports_path)
    end
  end

end
