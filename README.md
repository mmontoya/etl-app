ETL Starter Application
=======================

Rails application with just enough code to start AELOGICA's [ETL exercise](https://git.appexpress.io/open-source/etl-exercise).

### Instructions

1. Clone and install gems

     $ git clone git@gitlab.com:mmontoya/etl-app.git && cd etl-app && bundle install

2. Create the DB, copy the config file and run the migrations:

     $ cp config/database.yml.tmpl config/database.yml

     $ RAILS_ENV=development bundle exec bin/rails db:migrate

3. Run the application

     $  RAILS_ENV=development bin/bundle exec bin/rails s

4. In your browser upload the file:  lib/etl/spec/files/employees.csv

5. Click on "Import" and check the "employees" table in the DB.

6. To test create and migrate etl_test DB and:

      $ RAILS_ENV=test bundle exec rspec spec/requests/imports_request_spec.rb

Voila!
