class CreateImports < ActiveRecord::Migration[5.2]
  def change
    create_table :imports do |t|
      t.string  :notes
      t.string  :sumcheck
      t.string  :file
      t.boolean :done, default: false

      t.timestamps
    end
  end
end
