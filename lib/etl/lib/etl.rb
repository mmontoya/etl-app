require 'etl/version'
require 'etl/process'
require 'etl/etl_steps'
require 'etl/sources/generic_source'
require 'etl/targets/sql_targets'
require 'logger'
require 'date'
require 'time'

module Etl
  class Job
    def initialize(attrs)
      @steps = Etl::EtlSteps.new attrs
    end

    def run
      Etl::Process.call @steps
    end
  end
end

