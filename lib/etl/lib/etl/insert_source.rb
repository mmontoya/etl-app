require_relative 'sql_table'
require 'light-service'

module Etl
class InsertSource
  extend LightService::Action

  promises :insert_source

  executed do |ctx|
    ctx.after_etl = SqlTable.insert
  end
end
end
