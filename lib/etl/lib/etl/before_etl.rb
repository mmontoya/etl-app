require_relative 'sql_table'
require 'light-service'

module Etl
class BeforeEtl
  extend LightService::Action

  promises :before_etl

  executed do |ctx|
    ctx.before_etl = SqlTable.before
  end
end
end
