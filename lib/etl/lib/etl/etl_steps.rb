require 'json'

module Etl
  class EtlSteps
    def initialize(attrs)
      @attrs = attrs
      puts "etl_steps >>>>   #{@attrs}"
    end

    def before
      ['csv', 'json'].each do |dir|
        Dir.foreach(Rails.root.join('data', dir)) do |file|
          next unless File.directory? file
          if dir == 'json'
            content = File.open file
            data = JSON.parse content
          else
            data = CSV.parse(File.open(file, "r:windows-1251:utf-8"), headers: true, header_converters: :symbol, encoding: 'UTF-8')
          end
        end
      end
    end

    def after
      connection.query %[UPDATE employees SET note = 'WOW' WHERE evaluation > 0.5]
    end

    def secure_target
      connection.query @table_target unless @table_target.nil?
    end

    def reorder(line)
      line.each_with_object({}) do |(key, value), hash|
        if [:eid, :lat, :long, :evaluation].include? key
          hash[key] = value.to_f
        else
          hash[key] = value
        end
      end
    end

    def to_query(h)
      %[INSERT INTO units (eid, company,  name, code, lat, long, uuid, area, department, evaluation)
               VALUES (#{h[:eid]}, '#{h[:company]}', '#{h[:name]}', '#{h[:code]}', #{h[:lat]}, #{h[:long]},
               '#{h[:uuid]}', '#{h[:area]}', '#{h[:department]}', #{h[:evaluation]} )]
    end

    def sql_create
      %[CREATE TABLE IF NOT EXISTS employees (
             id serial PRIMARY KEY,
             eid int NOT NULL,
             company VARCHAR(200) NOT NULL,
             name VARCHAR(200) NOT NULL,
             code int NOT NULL,
             lat float NOT NULL,
             long float NOT NULL,
             uuid VARCHAR(200) NOT NULL,
             area VARCHAR(200) NOT NULL,
             department VARCHAR(200) NOT NULL,
             evaluation float NOT NULL,
             note VARCHAR(200),
             created_at timestamp(0) with time zone NOT NULL DEFAULT now()
         )]
  end
end
end
