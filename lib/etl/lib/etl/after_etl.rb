require_relative 'sql_table'
require 'light-service'

module Etl
class AfterEtl
  extend LightService::Action

  promises :after_etl

  executed do |ctx|
    ctx.after_etl = SqlTable.after
  end
end
end
