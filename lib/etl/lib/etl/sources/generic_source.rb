require 'etl/sources/csv_source'
require 'etl/sources/json_source'
require 'etl/sources/table_source'

module Etl
module Sources
  class GenericSource
    TYPES = {
      csv: CsvSource,
      json: JsonSource,
      table: TableSource
    }
    def self.for(type, attributes)
      (TYPES[type] || CsvSource).new(attributes)
    end
  end
end
end


