# File: csv_source.rb
require 'csv'

module Etl
module Sources
    class CsvSource
      def initialize(attributes)
        @csv = CSV.parse(File.open(attributes[:input_file], "r:windows-1251:utf-8"), headers: true, header_converters: :symbol, encoding: 'UTF-8')
      end

      def parse_source
        @csv.map do |data|
          data.to_hash
        end
      end
    end
end
end

