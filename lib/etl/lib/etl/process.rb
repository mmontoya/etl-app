require 'light-service'
require 'etl/prepare_target_table'
require 'etl/before_etl'
require 'etl/insert_source'
require 'etl/after_etl'

module Etl
class Process
  extend LightService::Organizer

  def self.call(steps)
    with(steps: steps).reduce(
        PrepareTargetTable,
        BeforeEtl,
        InsertSource,
        AfterEtl
      )
  end
end
end


Hi Guys!

Unfortunately I'm in the middle of several interviews and I don't have the time to attend the pending exercise. Anyhow, I appreciate the chance and it was really nice to meet you.
Good luck and have fun with your projects!

--mm
