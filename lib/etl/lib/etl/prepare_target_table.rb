require_relative 'sql_table'
require 'light-service'

module Etl
class PrepareTargetTable
  extend LightService::Action
  expects :steps
  promises :data

  executed do |ctx|
    #puts ">>>>> Context:   #{ctx}"
    ctx.data = SqlTable.create
    puts "DATTTAAAAA >>>>>  #{ctx.data}"
  end
end
end
