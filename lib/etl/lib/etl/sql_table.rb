require 'json'

module Etl
class SqlTable
  def self.before
    path = Rails.root.join('data')
    ['csv', 'json'].each_with_object([]) do |dir, acc|
      Dir.foreach(File.join(path,dir)) do |file|
        next if File.directory? file
        full_path = File.join(path,dir,file)
        if dir == 'json'
          content = File.read full_path
          data = JSON.parse content
          pro  = reorder_json(data)
          #acc << reorder_json(data)
        else
          data = CSV.parse(File.open(full_path, "r:windows-1251:utf-8"), headers: true, header_converters: :symbol, encoding: 'UTF-8')
          acc << reorder_csv(data)
        end
      end
      acc
    end
  end

  def self.after
    return false
    connection.query %[UPDATE units SET note = 'WOW' WHERE evaluation > 0.5]
  end

  def self.secure_target
    connection.query create
  end

  def self.reorder_json(data)
    data['units'].each_with_object([]) do |elem, row|
      acc = elem.reduce({}) do |memo, (k,v)|
        if ["name", "description", "price", "area"].include?(k)
          memo[k.to_sym] = v
        end
        memo
      end
      row << acc unless acc.empty?
    end
  end

  def self.reorder_csv(data)
    data.map do |row|
      row.to_hash
    end
  end

  def to_query(h)
    %[INSERT INTO units (price, name, description, area, uom)
      VALUES (#{h[:price]}, '#{h[:name]}', '#{h[:description]}', #{h[:area]}, '#{h[:uom]}'})]
  end

  def self.create
         %[CREATE TABLE IF NOT EXISTS units (
             id serial PRIMARY KEY,
             price  numeric(8,2),
             name   VARCHAR,
             description text,
             area double precision,
             uom character varying,
             created_at timestamp(0) with time zone NOT NULL DEFAULT now(),
             updated_at timestamp(0) with time zone NOT NULL DEFAULT now()
         )]
  end
end
end
