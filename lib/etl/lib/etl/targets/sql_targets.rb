module Etl
module Targets
  class SqlTargets
    def self.employees_table_target
      %[CREATE TABLE IF NOT EXISTS employees (
             id serial PRIMARY KEY,
             eid int NOT NULL,
             company VARCHAR(200) NOT NULL,
             name VARCHAR(200) NOT NULL,
             code int NOT NULL,
             lat float NOT NULL,
             long float NOT NULL,
             uuid VARCHAR(200) NOT NULL,
             area VARCHAR(200) NOT NULL,
             department VARCHAR(200) NOT NULL,
             evaluation float NOT NULL,
             note VARCHAR(200),
             created_at timestamp(0) with time zone NOT NULL DEFAULT now()
         )]
    end
  end
end
end

