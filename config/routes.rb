Rails.application.routes.draw do
  root to: "imports#index"
  resources :imports do
    collection do
      get 'etl_process'
    end
  end
end
