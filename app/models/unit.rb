# coding: utf-8
# frozen_string_literal: true

require 'etl'

 # price       | numeric(8,2)
 # name        | character varying
 # description | text
 # area        | double precision
 # uom         | character varying

class Unit < ApplicationRecord

  validates :name, presence: true

  # returns boolean
  def self.process_imports
    attributes = {connection: ActiveRecord::Base.connection}
    result = Etl::Job.new(attributes).run
    update_column(:done, true) if result
    result
  end

end

