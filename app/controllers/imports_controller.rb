class ImportsController < ApplicationController
  before_action :set_import, only: [:show, :update, :destroy]

  # GET /imports
  def index
    @units = Unit.all
  end

  # POST /imports
  # POST /imports.json
  def create
    #return render html: import_params["files"].original_filename.inspect
    full_params = import_params.merge(file: import_params["files"].original_filename)
    @import = Import.new(full_params)

    respond_to do |format|
      if @import.save
        format.html { redirect_to imports_path, notice: 'Import was successfully created.' }
        format.json { render :show, status: :created, location: @import }
      else
        format.html { redirect_to imports_path, notice: 'Something went wrong.' }
        format.json { render json: @import.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /imports/process
  def etl_process
    if Unit.process_imports
      redirect_to imports_path, notice: 'File was successfully imported.'
    else
      return render html: 'Something went wrong with the file.'
    end
  end

  # PATCH/PUT /imports/1
  # PATCH/PUT /imports/1.json
  def update
    respond_to do |format|
      if @import.update(import_params)
        format.html { redirect_to @import, notice: 'Import was successfully updated.' }
        format.json { render :show, status: :ok, location: @import }
      else
        format.html { render :edit }
        format.json { render json: @import.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /imports/1
  # DELETE /imports/1.json
  def destroy
    @import.destroy
    respond_to do |format|
      format.html { redirect_to imports_url, notice: 'Import was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_import
      @import = Import.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def import_params
      params.require(:import).permit(:notes, :sumcheck, :file, :files)
    end
end
